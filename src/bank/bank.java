package bank;

import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.io.Serializable;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.IOException;

public class bank implements Serializable
{
	private String name;
	private int lastaccno;
	private Map<Long,account> accountMap = new HashMap<Long,account>();
	
	bank(String n, int bankCode){
		this.name=n;
		this.lastaccno=bankCode*10000;
		}
		
	public enum Type
	{
		SAVINGS,
		CURRENT
		;
	}
		
	public String getName(){
		return this.name;
		}
		
	public int getBankCode(){
		return this.lastaccno/10000;
		}
		
/*	public int openSavingsAccount(String n, long openBal){
		account ac=new saving(++lastaccno,n,openBal);
		accountMap.put(ac.getAccountNo(),ac);
		return ac.getAccountNo;
		}
*/		
	public long openCurrentAccount(Type t, String n, long openBal){
		
		account ac=new current(++lastaccno,n,openBal);
		accountMap.put(ac.getAccountNo(),ac);
		return ac.getAccountNo();
		}
		
	private account getAccount(long acno){
		account ac = accountMap.get(acno);
		return ac;
		}
		
	
	public void deposit(long acno,long amt){
		 getAccount(acno).deposit(amt);
		 }
		 
	public boolean withdraw(long acno, long amt){
		return getAccount(acno).with(amt);
		}
		
	public void display(int acno){
		getAccount(acno).display();
		}
		
	public void display(long acno){
		getAccount(acno).display();
		}
		
	public void printpassbook(long acno){
		getAccount(acno).printpassbook();
		}
		
	public void listAccounts(){
		Collection <account>accounts= accountMap.values();
		System.out.println("account list for bank: "+name);
		for (account ac : accounts){
			System.out.println(ac);
		}
		System.out.println("End Of Account list");
	}
	
	public void save() throws IOException
	{
		String bankFileName = this.name + ".bank";
		FileOutputStream fos = new FileOutputStream(bankFileName);
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(this);
		oos.close();
		fos.close();
	}
	
	public bank load(String bankFileName) throws IOException, ClassNotFoundException
	{
		FileInputStream fis = new FileInputStream(bankFileName + ".bank");
		ObjectInputStream ois = new ObjectInputStream(fis);
		bank bk=(bank)ois.readObject();
		ois.close();
		fis.close();
		return bk;
	}
	}

package bank;
public class Negamt extends Exception
{
	private long amt;
	private account acc;
	Negamt(String msg,account ac,long amt)
	{
		super(msg);
		this.amt=amt;
		this.acc=ac;
	}
	public long getamt()
	{
		return this.amt;
	}
	public account getacc()
	{
		return this.acc;
	}
	public String toString()
	{
		return super.toString() + ":" + this.amt + ";" + this.acc;
	}
}
	
